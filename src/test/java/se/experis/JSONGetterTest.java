package se.experis;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class JSONGetterTest {

    @Test
    void nothing() {

    }

    @Test
    void makeASuccessfulGetRequest() {
        JSONGetter getter = new JSONGetter();
        assertTrue(getter.makeGetRequest("https://www.anapioficeandfire.com/api"));
        assertTrue(getter.isRequestOK());
    }

    @Test
    void makeAFailingGetRequest() {
        JSONGetter getter = new JSONGetter();
        assertFalse(getter.makeGetRequest("https://www.anapioficeandfire.com/ape"));
        assertFalse(getter.isRequestOK());
    }

    @Test
    void makeACharacterRequest() {
        CharacterGetter character = new CharacterGetter("https://anapioficeandfire.com/api/characters/583");
        assertTrue(character.isValid());
        assertEquals("Jon Snow", character.getName());
        assertEquals("Male", character.getGender());
        assertEquals("Northmen", character.getCulture());
    }

    @Test
    void getAllNamesFromCharacterHouse() {
        CharacterGetter character = new CharacterGetter("https://anapioficeandfire.com/api/characters/583");
        ArrayList<String> houseMembers = character.getListOfAllHouseMembers();
        assertEquals(88, houseMembers.size());
    }

    @Test
    void getNameListFromCharacterWithoutHouse() {
        CharacterGetter character = new CharacterGetter("https://anapioficeandfire.com/api/characters/21");
        ArrayList<String> houseMembers = character.getListOfAllHouseMembers();
        assertEquals(0, houseMembers.size());
    }

    @Test
    void getNameListFromCharacterWithTwoHouses() {
        CharacterGetter character = new CharacterGetter("https://anapioficeandfire.com/api/characters/47");
        ArrayList<String> houseMembers = character.getListOfAllHouseMembers();
        assertEquals(26, houseMembers.size());
    }

    @Test
    void getBookTitleAndPublisher() {
        BookGetter book = new BookGetter("https://www.anapioficeandfire.com/api/books/1");
        assertTrue(book.isValid());
        assertEquals("A Game of Thrones", book.getTitle());
        assertEquals("Bantam Books", book.getPublisher());
    }

    @Test
    void tryLoadingCharacterAsBook() {
        BookGetter book = new BookGetter("https://anapioficeandfire.com/api/characters/583");
        assertFalse(book.isValid());
        assertThrows(IllegalStateException.class, () -> book.getTitle());
        assertThrows(IllegalStateException.class, () -> book.getPublisher());
    }

    @Test
    void tryLoadingBookAsCharacter() {
        CharacterGetter character = new CharacterGetter("https://www.anapioficeandfire.com/api/books/1");
        assertFalse(character.isValid());
        assertThrows(IllegalStateException.class, () -> character.getListOfAllHouseMembers());
        assertThrows(IllegalStateException.class, () -> character.getName());
        assertThrows(IllegalStateException.class, () -> character.getGender());
        assertThrows(IllegalStateException.class, () -> character.getCulture());

    }

    @Test
    void getPovCharactersFromBook() {
        BookGetter book = new BookGetter("https://www.anapioficeandfire.com/api/books/1");
        assertTrue(book.isValid());
        ArrayList<String> characterNames = book.getAllPovCharacters();
        assertEquals(9, characterNames.size());
    }
}