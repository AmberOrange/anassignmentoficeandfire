package se.experis;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Program {
    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        int inputID = -1;

        while(inputID <= 0) {
            try {
                System.out.printf("Enter a character id:%n>");
                inputID = inputScanner.nextInt();
                if(inputID <= 0) {
                    System.out.println("Please enter a number larger than 0");
                }
            } catch(InputMismatchException e) {
                System.out.println("Please enter a valid number");
                inputScanner.nextLine();
            }
        }

        inputScanner.nextLine();

        CharacterGetter character = new CharacterGetter("https://anapioficeandfire.com/api/characters/"+inputID);
        if(character.isValid()) {
            System.out.printf("Name: %s%nGender: %s%nCulture: %s%n",
                    character.getName(),
                    character.getGender(),
                    character.getCulture());

            boolean userWantsAllegianceList;
            System.out.printf("%nList characters with same sworn allegiances?%n>");

            while(true) {
                String userAnswer = inputScanner.nextLine();
                if(userAnswer.toLowerCase().contains("y")) {
                    userWantsAllegianceList = true;
                    break;
                } else if(userAnswer.toLowerCase().contains("n")) {
                    userWantsAllegianceList = false;
                    break;
                } else {
                    System.out.printf("Enter yes or no (y/n):%n>");
                }
            }

            if(userWantsAllegianceList)
            {
                System.out.println("Looking for sworn house members (may take a few seconds)...");
                ArrayList<String> houseMembers = character.getListOfAllHouseMembers();
                if(!houseMembers.isEmpty()) {
                    System.out.println("Sworn members of this character's house(s):");
                    for (String name :
                            houseMembers) {
                        System.out.println(name);
                    }
                } else {
                    System.out.println("No houses found");
                }
            }

        } else {
            System.out.println("This character does not exist");
        }

        BookGetter book;
        int i = 1;
        while((book = new BookGetter("https://www.anapioficeandfire.com/api/books/" + i)).isValid()) {
            if(book.getPublisher().contains("Bantam Books")) {
                ArrayList<String> povCharacters = book.getAllPovCharacters();
                if(!povCharacters.isEmpty()) {
                    System.out.printf("Point of View characters from \"%s\":%n", book.getTitle());
                    for (String name :
                            povCharacters) {
                        System.out.println(name);
                    }
                    System.out.print("\n");
                }
            }
            i++;
        }
    }
}
