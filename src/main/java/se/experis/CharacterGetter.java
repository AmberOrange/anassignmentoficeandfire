package se.experis;

import org.json.JSONException;

import java.util.ArrayList;

public class CharacterGetter extends JSONGetter {
    public CharacterGetter(String requestURL) {
        super(requestURL);
    }

    @Override
    public boolean makeGetRequest(String requestURL) {
        return super.makeGetRequest(requestURL) && json.has("allegiances");
    }

    @Override
    public boolean isValid() {
        return isRequestOK() && json.has("allegiances");
    }

    public ArrayList<String> getListOfAllHouseMembers() {
        ArrayList<String> houseMembers = new ArrayList<>();

        // Check if the getter is valid
        if(!isValid()){
            throw new IllegalStateException("The character getter is not valid.");
        }

        try {
            for (Object allegianceName :
                    json.getJSONArray("allegiances")) {
                JSONGetter allegiance = new JSONGetter((String) allegianceName);

                if (allegiance.isValid() && allegiance.json.has("swornMembers")) {
                    for (Object memberName :
                            allegiance.json.getJSONArray("swornMembers")) {
                        CharacterGetter memberGetter = new CharacterGetter((String) memberName);
                        if (memberGetter.isValid()) {
                            houseMembers.add(memberGetter.json.getString("name"));
                        }
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return houseMembers;
    }

    public String getName() {
        if(!isValid()){
            throw new IllegalStateException("The character getter is not valid.");
        }
        return json.getString("name");
    }

    public String getGender() {
        if(!isValid()){
            throw new IllegalStateException("The character getter is not valid.");
        }
        return json.getString("gender");
    }

    public String getCulture() {
        if(!isValid()){
            throw new IllegalStateException("The character getter is not valid.");
        }
        return json.getString("culture");
    }
}
