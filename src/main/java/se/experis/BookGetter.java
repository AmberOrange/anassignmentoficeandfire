package se.experis;

import java.util.ArrayList;

public class BookGetter extends JSONGetter {
    public BookGetter(String requestURL) {
        super(requestURL);
    }

    public BookGetter() {
    }

    @Override
    public boolean makeGetRequest(String requestURL) {
        return super.makeGetRequest(requestURL) && json.has("publisher");
    }

    @Override
    public boolean isValid() {
        return super.isValid() && json.has("publisher");
    }


    public String getTitle() {
        if(!isValid()) {
            throw new IllegalStateException("The book getter is not valid.");
        }
        return json.getString("name");
    }

    public String getPublisher() {
        if(!isValid()) {
            throw new IllegalStateException("The book getter is not valid.");
        }
        return json.getString("publisher");
    }

    public ArrayList<String> getAllPovCharacters() {
        if(!isValid()) {
            throw new IllegalStateException("The book getter is not valid.");
        }

        ArrayList<String> characterList = new ArrayList<>();
        for (Object characterURL :
                json.getJSONArray("povCharacters")) {
            CharacterGetter character = new CharacterGetter((String)characterURL);
            if(character.isValid()) {
                characterList.add(character.json.getString("name"));
            }
        }
        return characterList;
    }
}
