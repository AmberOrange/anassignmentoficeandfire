## An _API of Ice and Fire_ Assignment
This application fetches information from a REST API and presents it in the console ([https://anapioficeandfire.com/](https://anapioficeandfire.com/)).
The project uses Java14, Gradle and JUnit5.